import { Module, Mutation, VuexModule, } from 'vuex-module-decorators'
import Vue from 'vue'

interface User {
  avatar: string
  name: string
  slug: string
  bio: string
  certificate: {}
}

@Module({
  name: 'user-store',
  stateFactory: true,
  namespaced: true
})
export default class UserStore extends VuexModule {

  _userMap: Object = {}
  _activeUser: string | undefined = ""

  get user(): any {
    if (this._activeUser)
      return this._userMap[this._activeUser as keyof Object]
    return undefined
  }

  get itemList() : User[] {
    return [...Object.values(this._userMap)]
  }

  @Mutation
  setUser(slug : string | undefined) {
    if (slug === '' || slug === undefined || slug in this._userMap)
      this._activeUser=slug
  }

  @Mutation
  createUser(newUser : any) {
    Vue.set(this._userMap, newUser.slug, newUser)
  }

  @Mutation
  deleteUser(slug: string) {
    if (this._activeUser === slug)
      this._activeUser = undefined
    Vue.delete(this._userMap, slug)
  }
}
