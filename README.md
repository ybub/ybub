# ybub
ybub is a peer-to-peer card game application.

## Goals
What this project aims to be:
- A tool to play card games online with multiple players.
- Build custom decks of cards for popular card collecting games. (ie: MTG)
- Connect directly to your friends using peer to peer systems.
- Web based, no install, cross-platform, aimed at the desktop experience.
- Free as in freedom, no accouts, emails or premium version.

## Play on ybub now
Try our live version here at:

https://ybub.gitlab.io/ybub/

## Build Setup

#### Install dependencies
```bash
$ npm install
```

#### Serve with hot reload at localhost:3000
```bash
$ npm run dev
```

#### Build for production
```bash
$ npm run generate
```
