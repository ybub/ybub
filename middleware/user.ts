import { Middleware } from '@nuxt/types'

const userCheck: Middleware = ({route, store, redirect}) => {
  if (route.name === 'user')
    return

  // Verify that a user is logged in
  if (!store.getters['user-store/user']) {
    // redirect with follow url
    return redirect(`/user?r=${route.fullPath}`)
  }
}

export default userCheck
